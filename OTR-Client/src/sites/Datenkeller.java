package sites;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Exceptions.WaitingException;
import core.main.Debug;
import core.main.OTRClient;
import downloader.Browser;
import downloader.Download;
import otr.OtrKeyResult;

public class Datenkeller extends Observable implements DownloadSeite,Downloadlimit {

	private DLogger dLogger;
	
	private boolean preparing;
	
	private Date date;
	
	private String status;

	public Datenkeller(DLogger logger) {
		dLogger = logger;
		date = null;
		status = "Downloadprozess gestartet";
	}

	@Override
	public boolean download(OtrKeyResult result, Browser browser) {
		if(Boolean.parseBoolean(OTRClient.getConf().load("directdownload"))) {
			Debug.println("Datenkeller does not support Direct Download !");
			dLogger.println("Datenkeller does not support Direct Download !");
			browser.setAvailable(true);
			return false;
		}
		
		preparing = true;
		// Get WebDriver
		WebDriver driver = browser.getWebDriver();
		// Navigate to URL
		driver.get(result.getDatenkeller());
		// Warte bis das Logo geladen wurde
		status = "Warten bis Seite geladen wurde";
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Download")));

		driver.findElement(By.partialLinkText("Download")).click();
		dLogger.println("Download geklickt");

		if(!driver.getTitle().contains("Download:") && !driver.getTitle().contains("warten:"))
			driver.close();
		
		String subfenster = null;
		Set<String> handels = driver.getWindowHandles();
		
		haupt:
		for (int i = 0; i < 3; i++) {
			status = "Downloadschleife";
			//Subfenster suchen
			
			SimpleDateFormat waiting = new SimpleDateFormat("HH:mm");
			
			for (String s : handels) {
				driver.switchTo().window(s);
				status = "Subfenster suchen";
				if (driver.getTitle().contains("Download:")) {
					dLogger.println(driver.getTitle());
					subfenster = s;
					break haupt;
				} else if (driver.getTitle().contains("warten:")) {
					dLogger.println(driver.getTitle());
					subfenster = s;
					break haupt;
				} else if(driver.getPageSource().contains("h�chstens")){
					String datum = driver.getPageSource().split("warte bis ")[1].split(" zum n�chsten")[0];
					System.out.println("Warten bis : "+datum);
					try {
						Date date3 = waiting.parse(datum);
						Calendar date2 = Calendar.getInstance();
						date2.setTime(date3);
						Calendar calendar = Calendar.getInstance();
						calendar.set(Calendar.HOUR_OF_DAY, date2.get(Calendar.HOUR_OF_DAY));
						calendar.set(Calendar.MINUTE, date2.get(Calendar.MINUTE));
						date = new Date(calendar.getTimeInMillis());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
			}
			status = "Subfenster nicht gefunden";
			
			//Wenn gefunden Schleife verlassen sonst 10 Sekunden warten
			
			try{
			Thread.sleep(10000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		status = "Downloadschleife verlassen";
		
		//Erneute �berpr�fung ob es gefunden wurde
		
		if(subfenster == null){
			if(date != null){
				throw new WaitingException("Wartezeit bis "+date.toString());
			}else{
				throw new IllegalStateException("Subfenster nach 3 Versuchen nicht gefunden");
			}
		}
		dLogger.println(driver.getTitle());
		
		//Warteschlange
		int tries = 0;
		
		while (true) {
			status = "Downloadwarteschlange ...";
			if (driver.getTitle().contains("Download:")) {
				break;
			}
			if(result.getSize() > 1 && tries == 180) //30Minuten gewartet (1 = 10 Sekunden)
				throw new IllegalStateException("Nach 30 Minuten noch in Warteschleife. Versuche n�chsten Mirror");

			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			tries++;
		}

		status = "Downloadbutton klicken";
		try {
			driver.findElement(By.xpath("/html/body/div[2]/div/div/div[2]/a[1]")).click();
		} catch (NoSuchElementException e) {
			System.out.println("OTRKEY : " + result.getOtrkey());
			Debug.println("Datenkeller : Downlink klicken fehlgeschlagen");
			dLogger.println("Datenkeller : Downlink klicken fehlgeschlagen");
			e.printStackTrace(dLogger.getWriter());
		}

		try {
			Thread.sleep(20000);
		} catch (InterruptedException e1) {
			System.out.println("OTRKEY : " + result.getOtrkey());
			e1.printStackTrace();
			e1.printStackTrace(dLogger.getWriter());
		}
		
		status = "Downloading...";

		driver.navigate().to("about:blank");

		preparing = false;
		browser.setAvailable(true);
		browser.incDownloads();
		
		//Download als aktiv stellen
		
		setChanged();
		notifyObservers(true);
		
		//10 Sekunden Puffer
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		//Downloadbeobachter
		
		if(Download.Downloadwatch(result,dLogger)){
			browser.decDownloads();
			return true;
		}else{
			throw new IllegalStateException("Downloaddatei ist seit 45 Sekunden nicht gr��er geworden");
		}
	}

	@Override
	public boolean isPreparing() {
		return preparing;
	}

	@Override
	public Date getAvailableDate() {
		return date;
	}
	
	@Override
	public String getStatus() {
		return status;
	}

}
