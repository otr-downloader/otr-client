package sites;

import java.util.Observable;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.main.OTRClient;
import downloader.Browser;
import downloader.Download;
import otr.OtrKeyResult;

public class OTRRatte extends Observable implements DownloadSeite{
	
	private DLogger dLogger;
	
	private boolean preparing;
	
	private String status;

	public OTRRatte(DLogger logger) {
		this.dLogger = logger;
	}
	
	@Override
	public boolean download(OtrKeyResult result, Browser browser) {
		if(Boolean.parseBoolean(OTRClient.getConf().load("directdownload"))) {
			dLogger.println("OTRRATTE : DD NOT SUPPORTED");
			return false;
		}
		
		preparing = true;
		//Holt sich den WebDriver vom Browser Objekt
		WebDriver driver = browser.getWebDriver();
		//L�dt den Link f�r OtrRatte
		driver.get(result.getOtrratte());
		//Extraiert den OTRKEY Downloaddateiname
		String otrkey = driver.getCurrentUrl().split("searchstring=")[1];
		//Wartet auf den Twittertext auf der Seite
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("Twitter")));
		//Download klicken
		driver.findElement(By.linkText("Otrkey herunterladen")).click();
		
		//DirectDownload ?
		if(Boolean.parseBoolean(OTRClient.getConf().load("directdownload"))) {
			
		WebElement container = driver.findElement(By.className("main"));
		String url = container.findElement(By.tagName("input")).getAttribute("value");
		
		//Browser erneut verf�gbar
		driver.navigate().to("about:blank");
		preparing = false;
		browser.setAvailable(true);
		
		dLogger.println("DirectDownload starting ...");
		
		System.out.println(url);
		
		return Download.startDirectDownload(url, result);
		}
		
		//Download klicken
		driver.findElement(By.partialLinkText("Download starten")).click();
		
		//Auf blanke Seite wechseln
		driver.navigate().to("about:blank");
		
		//Download active
		
		preparing = false;
		browser.incDownloads();
		browser.setAvailable(true);
		
		//Download als aktiv kennzeichnen
		setChanged();
		notifyObservers(true);
		
		OtrKeyResult rKeyResult = result.clone();
		rKeyResult.setOtrkey(otrkey);
		
		dLogger.println("OTRKEY : "+rKeyResult.getOtrkey());
		
		if(Download.Downloadwatch(rKeyResult, dLogger)){
			browser.decDownloads();
			return true;
		}else{
			throw new IllegalStateException("Downloaddatei ist seit 45 Sekunden nicht gr��er geworden");
		}
	}

	@Override
	public boolean isPreparing() {
		return preparing;
	}

	@Override
	public String getStatus() {
		return status;
	}

}
