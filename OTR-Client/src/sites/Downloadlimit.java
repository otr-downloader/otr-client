package sites;

import java.util.Date;

public interface Downloadlimit {

	/**
	 * Gibt zur�ck wann der Download fortgesetzt werden kann
	 * @return Date oder null, wenn es keine Wartezeit gibt
	 */
	public Date getAvailableDate();
	
}
