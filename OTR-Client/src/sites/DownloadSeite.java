package sites;

import downloader.Browser;
import otr.OtrKeyResult;

/**
 * Interface f�r jede Downloadseite
 * @author Konstantin Sp�th
 *
 */
public interface DownloadSeite{

	/**
	 * Download auf einer Seite
	 * @param result OTRKEYResult 
	 * @param browser Browser der benutzt werden soll
	 * @return War der Download erfolgreich
	 */
	public abstract boolean download(OtrKeyResult result, Browser browser);
	
	/**
	 * Gibt zur�ck ob der Download in der Verbereitungsphase bis zum Start des Downloads ist
	 * @return
	 */
	public abstract boolean isPreparing();
	
	/**
	 * Gibt den aktuellen Status in der Downloadbearbeitung zur�ck
	 * @return
	 */
	public String getStatus();
	
}
