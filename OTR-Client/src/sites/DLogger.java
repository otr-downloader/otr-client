package sites;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.Date;

import core.main.OTRClient;
import otr.OtrKeyResult;

public class DLogger{
	
	private PrintWriter writer;
	
	public DLogger(){
		DateFormat formatfolder = DateFormat.getDateInstance(DateFormat.SHORT);
		DateFormat format = DateFormat.getTimeInstance(DateFormat.MEDIUM);
		File dir = new File(OTRClient.getConf().load("logdir")+File.separator+formatfolder.format(new Date())+File.separator+"downloads");
		dir.mkdirs();
		try {
			File f = new File(dir.getAbsolutePath()+File.separator+"log_"+format.format(new Date()).replace(":", "-")+".txt");
			if(f.exists()){
				f = new File(dir.getAbsolutePath()+File.separator+"log_"+format.format(new Date()).replace(":", "-")+"."+(10*Math.random())+".txt");
			}
			writer = new PrintWriter(new FileWriter(f));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public DLogger(OtrKeyResult keyResult) {
		DateFormat formatfolder = DateFormat.getDateInstance(DateFormat.SHORT);
		DateFormat format = DateFormat.getTimeInstance(DateFormat.MEDIUM);
		File dir = new File(OTRClient.getConf().load("logdir")+File.separator+formatfolder.format(new Date())+File.separator+"downloads");
		dir.mkdirs();
		try {
			File f = new File(dir.getAbsolutePath()+File.separator+"log_"+format.format(new Date()).replace(":", "-")+"."+neutralize(keyResult.getOtrkey())+".txt");
			if(f.exists()){
				f = new File(dir.getAbsolutePath()+File.separator+"log_"+format.format(new Date()).replace(":", "-")+"."+neutralize(keyResult.getOtrkey())+"."+(10*Math.random())+".txt");
			}
			writer = new PrintWriter(new FileWriter(f));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public DLogger(String filename) {
		File logfile = new File(filename);
		if(logfile.exists()) {
			DateFormat format = DateFormat.getTimeInstance(DateFormat.MEDIUM);
			logfile = new File("log_"+format.format(new Date()).replace(":", "-")+"."+(10*Math.random())+".txt");
		}
		try {
			writer = new PrintWriter(new FileWriter(logfile));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void println(String x){
		writer.println(x);
		writer.flush();
	}
	
	public PrintWriter getWriter(){
		return writer;
	}
	
	private String neutralize(String otrkeyname){
		String name = "";
		String[] split = otrkeyname.split("\\.");
		for (int i = 0; i < split.length; i++) {
			switch (split[i]) {
			case "mpg":
			case "avi":
			case "HQ":
			case "HD":
			case "mp4":
			case "otrkey":
				break;
			default:
				name = name.concat(split[i] + ".");
				break;
			}
		}
		return name;
	}
	
}
