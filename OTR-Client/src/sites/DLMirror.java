package sites;

import java.io.File;
import java.util.Observable;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.main.OTRClient;
import downloader.Browser;
import downloader.Download;
import otr.OtrKeyResult;

public class DLMirror extends Observable implements DownloadSeite{

	private DLogger dLogger;
	
	private boolean preparing;
	
	private String status;
	
	public DLMirror(DLogger logger) {
		this.dLogger = logger;
	}
	
	@Override
	public boolean download(OtrKeyResult result, Browser browser) {
		
		preparing = true;
		
		//L�dt den WebDriver vom Browser Objekt
		WebDriver driver = browser.getWebDriver();
		
		//L�dt die DLMirror Url
		driver.get(result.getDlmirror());
		
		//Wartet bis die Copyright Sektion geladen wurde
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("copyright")));
		
		driver.findElement(By.name("createlink")).click();
		
		//Wartet bis das Downloadform geladen wurde
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("download_form")));
		
		int aus = Integer.MAX_VALUE;
		int form = 0;
		int id = 0;
		for(WebElement e : driver.findElements(By.tagName("div"))) {
			if(Pattern.matches("[0-9]* % Auslastung", e.getText())) {
				if(aus < Integer.parseInt(e.getText().split(" % Auslastung")[0])) {
					aus = Integer.parseInt(e.getText().split(" % Auslastung")[0]);
					form = id;
				}
				id++;
			}
		}
		
		//Da Auslastung mehrfach gefunden wird
		WebElement dform = driver.findElements(By.id("download_form")).get(form/3);
		String url = dform.findElement(By.tagName("input")).getAttribute("value");
		
		//Download through Java
		if(Boolean.parseBoolean(OTRClient.getConf().load("directdownload"))) {
			//Browser erneut verf�gbar
			driver.navigate().to("about:blank");
			preparing = false;
			browser.setAvailable(true);
			
			dLogger.println("DirectDownload starting ...");
			
			return Download.startDirectDownload(url, result);
		}
		//***
		try {
			driver.get(url);
		} catch(TimeoutException e) {
			if(!Download.partfileExists(result)) {
				throw e;
			}
		}
		
		
		//10 Sekunden Puffer
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			e1.printStackTrace(dLogger.getWriter());
		}
		//Auf blanke Seite wechseln
		driver.navigate().to("about:blank");
		
		//Beendet die Vorbereitungsphase
		preparing = false;
		//F�gt dem Browser einen Download hinzu
		browser.incDownloads();
		//Setzt die Browserverf�gbarkeit auf true
		browser.setAvailable(true);
		
		//Download als aktiv kennzeichnen
		setChanged();
		notifyObservers(true);
		
		if(Download.Downloadwatch(result, dLogger)){
			browser.decDownloads();
			return true;
		}else{
			throw new IllegalStateException("Downloaddatei ist seit 45 Sekunden nicht gr��er geworden");
		}
	}

	@Override
	public boolean isPreparing() {
		return preparing;
	}

	@Override
	public String getStatus() {
		return status;
	}


}
