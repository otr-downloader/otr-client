package Exceptions;

public class WaitingException extends IllegalStateException{
	 /**
	 * 
	 */
	private static final long serialVersionUID = -5272715722032790581L;

    public WaitingException() {}

    public WaitingException(String message)
    {
       super(message);
    }
}
