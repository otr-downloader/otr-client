package transport;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.NoRouteToHostException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import core.main.Debug;
import core.main.OTRClient;

/**
 * FTP Klasse
 * 
 * @author Konstantin Sp�th
 *
 */
public class FTP implements FileTransferProtocoll {

	String host, user, pw, dir;

	/**
	 * Benutzerdefiniertes FTP Objekt erstellen
	 * 
	 * @param host
	 *            HOST/Adresse
	 * @param user
	 *            Username
	 * @param pw
	 *            Passwort
	 * @param dir
	 *            Verzeichnis
	 */
	public FTP(String host, String user, String pw, String dir) {
		this.host = host;
		this.user = user;
		this.pw = pw;
		this.dir = dir;
	}

	/**
	 * Inizializiert mit den Standart Parametern
	 */
	public FTP() {
		host = OTRClient.getConf().load("ftphost");// 192.168.2.110 - mediaftp.ddns.net
		user = OTRClient.getConf().load("ftpuser"); // Server
		pw = OTRClient.getConf().load("ftppw"); // pw = 1909#1909
		dir = OTRClient.getConf().load("ftpdir"); // "/Warteschlange/"
	}

	/**
	 * Sendet die Datei an die vordefinierte Adresse und Verzeichnis
	 * 
	 * @param f
	 */
	public void send(File f) {

		FTPClient ftp = new FTPClient();
		FTPClientConfig config = new FTPClientConfig();
		ftp.configure(config);
		try {
			ftp.connect(host);
			ftp.login(user, pw);
			ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
			if (Boolean.parseBoolean(OTRClient.getConf().load("ftpactive"))) {
				ftp.enterLocalActiveMode();
			} else {
				ftp.enterLocalPassiveMode();
			}

			int reply = ftp.getReplyCode();

			if (!FTPReply.isPositiveCompletion(reply)) {
				ftp.disconnect();
				System.err.println("FTP server refused connection.");
				System.err.println("Fail");
				return;
			} else {
				Debug.println("Verbindung hergestellt");
			}

			ftp.changeWorkingDirectory(dir);
			boolean exists = false;

			for (FTPFile fftp : ftp.listFiles()) {
				if (fftp.getName().equals(f.getName())) {
					exists = true;
				}
			}

			if (!exists) {
				InputStream input = new FileInputStream(f);
				if (ftp.storeFile(f.getName() + ".part", input)) {
					ftp.sendSiteCommand("chmod 777 " + f.getName() + ".part");
					ftp.rename(f.getName() + ".part", f.getName());
					input.close();
					f.delete();
					System.out.println("Datei gesendet");
					OTRClient.getLog().println(f.getName() + " : Datei gesendet");
				} else {
					System.err.println("Fehler beim Senden der Datei");
					OTRClient.getLog().println(f.getName() + " : Fehler beim Senden der Datei");
					input.close();
				}
			}

			ftp.logout();

		} catch (NoRouteToHostException e) {
			e.printStackTrace(OTRClient.getLog().getWriter());
			Debug.println("Keine Route zum Host " + host + " m�glich (Host offline)");
		} catch (SocketException e) {
			e.printStackTrace();
			Debug.println("Verbindung zu " + host + " fehlgeschlagen");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}