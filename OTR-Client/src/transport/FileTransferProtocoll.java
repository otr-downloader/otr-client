package transport;

import java.io.File;

public interface FileTransferProtocoll {

	public abstract void send(File f);
	
}
