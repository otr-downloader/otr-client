package connector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.UUID;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import core.main.Debug;
import core.main.OTRClient;
import otr.OtrKeyResult;

public class Client implements Runnable{
	
	private SSLSocket socket;
	private Thread treceiver;
	private UUID uuid;

	public Client() {
		System.setProperty("javax.net.ssl.trustStore","truststore");
	    System.setProperty("javax.net.ssl.trustStorePassword", "31177#160799");
	    while(true) {
	    	if(connect())
	    		break;
	    	try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
		new TimeoutThread().start();
	}
	
	private void resolve(String msg) {
		String[] split = msg.split("!");
		switch(split[0]) {
		case "version":
			if(!split[1].equals(OTRClient.version)) {
				System.out.println("Server und Clientversion passen nicht zueinander");
				System.exit(-33);
			}else {
				sendText("ready");
			}
			break;
		case "download":
			switch(split[1]) {
			case "start":
				String keyResult = "";
				for(int i = 2;i<split.length;i++) {
					keyResult += split[i]+"!";
				}
				OTRClient.getThreadManager().getDownloadManager().download(OtrKeyResult.getResult(keyResult));
				break;
			}
			break;
		case "uuid":
			uuid = UUID.fromString(split[1]);
			break;
		}
	}
	
	private boolean connect() {
		return connect(OTRClient.getConf().load("serverip"));
	}
	
	private boolean connect(String host){
	    SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
        try {
			SSLSocket sslsocket = (SSLSocket) sslsocketfactory.createSocket(host, 60);
			this.socket = sslsocket;
			socket.setSoTimeout(30000);
			System.out.println("Connected");
			treceiver = new Thread(this);
			treceiver.start();
			if(uuid != null) {
				sendText("getuuid!"+uuid.toString());
			}
			sendText("version");
			return true;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch(ConnectException e){
			System.out.println("Verbindung fehlgeschlagen");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}catch (IOException e) {
			e.printStackTrace();
		}
        return false;
	}
	
	public void close() {
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void sendText(String text){
		try {
			PrintWriter writer = new PrintWriter(socket.getOutputStream());
			writer.println();
			writer.println("MESSAGE:"+text);
			writer.println();
			writer.flush();
			socket.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String text;
			while((text = reader.readLine())!= null){
				if(text.startsWith("MESSAGE:")){
					String msg = text.split("MESSAGE:")[1];
					if(!msg.equals("Test")) {
						Debug.println("Lese Nachricht : "+msg);
					}
					resolve(msg);
				}
			}
			
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Verbindung verloren");
			connect();
			//Verbindung verloren
		}
	}
	
	class TimeoutThread extends Thread{
		@Override
		public void run() {
			while(true) {
				sendText("Test");
				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
