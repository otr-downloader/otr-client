package tester;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import core.main.OTRClient;
import downloader.Browser;
import otr.OtrKeyResult;
import sites.DLMirror;
import sites.DLogger;
import sites.Datenkeller;
import sites.OTRRatte;

class DownloadTester {

	static OtrKeyResult keyResult;
	
	@BeforeAll
	public static void init() {
		new OTRClient(true);
		OTRClient.getConf().save("debug", "true");
		keyResult = new OtrKeyResult();
		
	}
	
	@AfterAll
	public static void deconstructor() {
		OTRClient.getConf().save("debug", "false");
	}
	
	@Test
	void testDatenkeller() {
		keyResult.setDatenkeller("https://otrkeyfinder.com/de/go-to-mirror?otrkey=Die_Tagesschau_vor_25_Jahren_1993_18.02.02_23-45_ardalpha_15_TVOON_DE.mpg.mp4.otrkey&destination=e71d1eb1d3e1759335caba79c47d03d620be488c3fa0ebab2f059d3d4d849f523A3MWQ2HFNc301f121b525a599");
		keyResult.setOtrkey("Die_Tagesschau_vor_25_Jahren_1993_18.02.02_23-45_ardalpha_15_TVOON_DE.mpg.mp4.otrkey");
		Datenkeller datenkeller = new Datenkeller(new DLogger("test.txt"));
		assertTrue(datenkeller.download(keyResult, new Browser()));
	}
	
	@Test
	void testDLMirror() {
		keyResult.setDlmirror("https://otrkeyfinder.com/de/go-to-mirror?otrkey=Ich_bin_ein_Star_Holt_mich_hier_raus_S12E08_18.02.01_22-15_rtl_75_TVOON_DE.mpg.HQ.avi.otrkey&destination=e169b7bfdc27e1eb9f3a43eb4d25ce6da259e8b90c969043379efb3c9dedcf99QMzwh_E0Ogc327c301a121b525d529c515f599f603b502a623");
		keyResult.setOtrkey("Ich_bin_ein_Star_Holt_mich_hier_raus_S12E08_18.02.01_22-15_rtl_75_TVOON_DE.mpg.HQ.avi.otrkey");
		DLMirror dlMirror = new DLMirror(new DLogger("test1.txt"));
		assertTrue(dlMirror.download(keyResult, new Browser()));
	}
	
	@Test
	void testOTRRatte() {
		keyResult.setOtrratte("https://otrkeyfinder.com/de/go-to-mirror?otrkey=Ich_bin_ein_Star_Holt_mich_hier_raus_18.02.02_22-15_rtl_60_TVOON_DE.mpg.HQ.avi.otrkey&destination=9b5c09ca31200af13d2491ade6fd8a222297260fef49d723bb7b3b8783787317DehFNRMnhkd502b301a121c525e515f599");
		keyResult.setOtrkey("Ich_bin_ein_Star_Holt_mich_hier_raus_S12E08_18.02.01_22-15_rtl_75_TVOON_DE.mpg.HQ.avi.otrkey");
		OTRRatte otrRatte = new OTRRatte(new DLogger("test2.txt"));
		assertTrue(otrRatte.download(keyResult, new Browser()));
	}
}
