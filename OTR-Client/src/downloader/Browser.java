package downloader;

import java.util.Date;
import java.util.Observable;

import org.openqa.selenium.WebDriver;

import core.main.OTRClient;

public class Browser extends Observable{
	
	private boolean available;
	
	private WebDriver webDriver;
	
	private int downloads;
	
	private Date LastUsage;
	
	public Browser(){
		webDriver = OTRClient.getnewBrowser();
		setAvailable(true);
	}
	
	public Browser(WebDriver webDriver) {
		this.webDriver = webDriver;
		setAvailable(true);
	}
	
	/**
	 * Beendet den Browser
	 */
	public void close(){
		if(downloads != 0){
			return;
		}
		setChanged();
		notifyObservers(this);
		OTRClient.closeBrowser(webDriver);
	}
	
	@Override
	public String toString(){
		String tmp = null;
		try{
			webDriver.toString();
		}catch(Exception e){
			tmp = "Unknown";
		}
		return tmp;
	}

	public WebDriver getWebDriver() {
		return webDriver;
	}

	public void setWebDriver(WebDriver webDriver) {
		this.webDriver = webDriver;
	}

	public synchronized boolean isAvailable() {
		return available;
	}

	public synchronized void setAvailable(boolean available) {
		this.available = available;
	}

	public int getDownloads() {
		return downloads;
	}

	/**
	 * F�gt den aktiven Downloads einen Download hinzu
	 */
	public void incDownloads(){
		downloads++;
	}
	
	/**
	 * Entfernt den aktiven Downloads einen Download
	 */
	public void decDownloads(){
		downloads--;
	}

	public Date getLastUsage() {
		return LastUsage;
	}

	public void setLastUsage(Date lastUsage) {
		LastUsage = lastUsage;
	}
	
}
