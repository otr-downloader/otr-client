package downloader;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.io.FileUtils;

import Exceptions.WaitingException;
import core.main.OTRClient;
import otr.OtrKeyResult;
import sites.DLMirror;
import sites.DLogger;
import sites.Datenkeller;
import sites.DownloadSeite;
import sites.Downloadlimit;
import sites.OTRRatte;

public class Download extends Observable implements Runnable, Observer{
	private OtrKeyResult otrKeyResult;
	private DLogger dLogger;
	private int anbieter;
	private boolean downloading;
	private boolean finished;
	private Date waitingdate;
	
	/**
	 * Download Objekt erstellen zu einem OtrKeyResult
	 * @param rOtrKeyResult OtrKeyResult
	 */
	public Download(OtrKeyResult rOtrKeyResult){
		otrKeyResult = rOtrKeyResult;
		dLogger = new DLogger(rOtrKeyResult);
		finished = false;
		downloading = false;
		waitingdate = null;
	}
	
	/**
	 * Downloadschleife
	 */
	@Override
	public void run() {
		System.out.println("Download : "+otrKeyResult.getOtrkey());
		boolean waiting = false;
		
		while(true){
			dLogger.println("Aufnahme : " + otrKeyResult.getOtrkey() + " : ");
			
			if(otrKeyResult.getDatenkeller() != null){
				Datenkeller dk = new Datenkeller(dLogger);
				dk.addObserver(this);
				anbieter = 0;
				if(download(dk)){
					break;
				}
			}if(otrKeyResult.getDlmirror() != null){
				DLMirror dl = new DLMirror(dLogger);
				dl.addObserver(this);
				anbieter = 1;
				if(download(dl)){
					break;
				}
			}if(otrKeyResult.getOtrratte() != null){
				OTRRatte otrr = new OTRRatte(dLogger);
				otrr.addObserver(this);
				anbieter = 2;
				if(download(otrr)){
					break;
				}
			}if(waitingdate != null){
				OTRClient.getThreadManager().getDownloadManager().getDKWaiter().addDownload(this);
				waiting = true;
				break;
			}
			
		}
		if(!waiting){
		//Download fertig
		dLogger.println("Fertig : "+otrKeyResult.getOtrkey());
		System.out.println("Fertig : "+otrKeyResult.getOtrkey());
		
		downloading = false;
		
		//Dekoder starten
		
		OTRClient.getProcessRunner().otrdecoder(new File(OTRClient.getConf().load("downloadir")+File.separator+otrKeyResult.getOtrkey()));
		
		dLogger.println("Dekodiere : "+otrKeyResult.getOtrkey());
		
		finished = true;
		setChanged();
		notifyObservers(true);
		}else{
			
		} 
	}
	
	/**
	 * Allgemeine Downloadinit Funktion
	 * @param s
	 * @return
	 */
	public boolean download(DownloadSeite s){
		Browser browser = OTRClient.getThreadManager().getDownloadManager().getBrowser();
		if(browser == null){
			dLogger.println("Kein Browser verf�gbar :/");
			dLogger.println("Erneuter Versuch in 90 Sekunden");
			try {
				Thread.sleep(90000);
				return download(s);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//Setzt die Browserverf�gbarkeit auf false
		browser.setAvailable(false);
		//DOWNLOAD
		try{
			return s.download(otrKeyResult,browser);
		}
		catch (WaitingException e){
			Downloadlimit downloadlimit = (Downloadlimit) s;
			waitingdate = downloadlimit.getAvailableDate();
			System.out.println("Waitingdate : "+ waitingdate.toString());
			browser.setAvailable(true);
		}
		catch(IllegalStateException e){
			e.printStackTrace(dLogger.getWriter());
			e.printStackTrace();
			if(downloading){
				browser.decDownloads();
				browser.setAvailable(true);
			}
			if(s.isPreparing()){
				browser.close();
			}
		}
		catch(Exception e){
			e.printStackTrace(dLogger.getWriter());
			e.printStackTrace();
			if(downloading){
				browser.decDownloads();
			}
			browser.close();
		}
		return false;
	}

	public int getAnbieter() {
		return anbieter;
	}
	
	public boolean isDownloading(){
		return downloading;
	}
	
	public OtrKeyResult getOtrKeyResult(){
		return otrKeyResult;
	}

	/**
	 * Downloading wird gesetzt
	 */
	@Override
	public void update(Observable o, Object arg) {
		downloading = (boolean) arg;
	}
	
	public static boolean Downloadwatch(OtrKeyResult keyResult, DLogger dLogger){
	
		String downloaddir = OTRClient.getConf().load("downloadir");
		File orgfile = new File(downloaddir+File.separator+keyResult.getOtrkey());
		
		//Partfile Chrome OR Firefox
		File partfile = null;
		if(OTRClient.getConf().load("browser").equals("chrome")){
			partfile = new File(downloaddir+File.separator+keyResult.getOtrkey()+".crdownload");
		}else{
			partfile = new File(downloaddir+File.separator+keyResult.getOtrkey()+".part");
		}
		
		dLogger.println("ORGFILE : "+orgfile.getAbsolutePath());
		dLogger.println("Filedownload : "+partfile.getAbsoluteFile());
		
		//Check if downloads starts in 60 seconds
		
		boolean started = false;
		
		for(int i = 0;i < 6;i++) {
			if(partfile.exists() || orgfile.exists()) {
				started = true;
				break;
			}
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if(!started)
			throw new IllegalStateException("Download innerhalb von 60 Sekunden nicht gestartet");
		//---
		
		long partsize = 0L;
		int partnum = 0;
		
		while(true){
			if(orgfile.exists() && orgfile.length() > 1000){
				dLogger.println("Download fertig");
				return true;
			}else{
				if(partnum == 3 && partsize == partfile.length()){
					dLogger.println("Partsize : "+partsize);
					dLogger.println("Partfile : "+partfile.length());
					orgfile.delete();
					partfile.delete();
					return false;
				}else if(partnum == 3){
					partsize = partfile.length();
					partnum = -1;
					dLogger.println("Gr��e : "+partfile.length());
				}
			}
			partnum++;
			
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static boolean partfileExists(OtrKeyResult keyResult) {
		String downloaddir = OTRClient.getConf().load("downloadir");
		//Partfile Chrome OR Firefox
		File partfile = null;
		if(OTRClient.getConf().load("browser").equals("chrome")){
			partfile = new File(downloaddir+File.separator+keyResult.getOtrkey()+".crdownload");
		}else{
			partfile = new File(downloaddir+File.separator+keyResult.getOtrkey()+".part");
		}
		return partfile.exists();
	}
	
	/**
	 * Start Direct Download
	 * @param url
	 * @return Success or not
	 */
	public static boolean startDirectDownload(String url,OtrKeyResult result) {
		try {
			FileUtils.copyURLToFile(new URL(url), new File(OTRClient.getConf().load("downloadir")+File.separator+result.getOtrkey()));
			return true;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Ist der Download fertig
	 * @return ^^
	 */
	public boolean isFinished() {
		return finished;
	}
	
	public Date getWaitingDate(){
		return waitingdate;
	}
	
}
