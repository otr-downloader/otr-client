package downloader;

import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import core.main.Debug;
import core.main.OTRClient;
import otr.OtrKeyResult;

public class DownloadManager implements Observer{

	private Vector<Download> downloads;
	
	private Vector<Browser> browsers;
	
	private ExecutorService executorService;
	
	//Variablen
	
	private int alloweddownloads;

	private int allowedbrowsers;
	
	private int allowedparoperations;
	
	//Objekte
	
	private DownloadWaiter dWaiter;
	
	public DownloadManager() {
		allowedparoperations = Integer.parseInt(OTRClient.getConf().load("allowedpoperations"));
		//Config laden
		alloweddownloads = Integer.parseInt(OTRClient.getConf().load("alloweddownloads"));
		allowedbrowsers = Integer.parseInt(OTRClient.getConf().load("allowedbrowsers"));
		
		//Executer laden
		
		if (alloweddownloads == -1) {
			executorService = Executors.newCachedThreadPool();
		} else {
			executorService = Executors.newFixedThreadPool(alloweddownloads);
		}
		
		//Variablen initalisieren
		
		downloads = new Vector<Download>();
		browsers = new Vector<Browser>();
		
		//Threads starten
		
		new BrowserCleaner().start();
		dWaiter = new DownloadWaiter();
		dWaiter.start();
	}
	
	/**
	 * Startet anhand eines OtrKeyResult Objekt einen entsprechenden Download
	 * @param keyResult
	 */
	public void download(OtrKeyResult keyResult) {
		availibility();
		if(downloads.size()<alloweddownloads) {
			Download d = new Download(keyResult);
			d.addObserver(this);
			downloads.add(d);
			executorService.execute(d);
		}else {
			throw new IllegalStateException("Anzahl gleichzeitig erlaubter Downloads erreicht");
		}
	}
	
	public void availibility() {
		OTRClient.getThreadManager().getClient().sendText("maxop!"+allowedparoperations);
	}
	
	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg1 instanceof Browser){
			browsers.remove(arg1);
		}else if(arg0 instanceof Download && (boolean)arg1 == true) {
			downloads.remove((Download)arg0);
		}
	}
	
	/**
	 * Gibt einen verf�gbaren bzw. neuen Browser zur�ck
	 * @return Browserobjekt
	 */
	public synchronized Browser getBrowser(){
		for(Browser b : browsers){
			if(b.isAvailable()){
				b.setLastUsage(new Date());
				return b;
			}
		}
		if(browsers.size() < allowedbrowsers){
			Browser b = new Browser();
			b.addObserver(this);
			b.setLastUsage(new Date());
			browsers.addElement(b);
			return b;
		}
		return null;
	}
	
	public class DownloadWaiter extends Thread{
		
		private Vector<Download> wdownloads = new Vector<Download>();
		
		public void run() {
			while(true) {
				Date akt = new Date();
				Vector<Download> tmp = new Vector<Download>(wdownloads);
				for(Download d : tmp) {
					if(d.getWaitingDate().before(akt)&&downloads.size()<alloweddownloads){
						Debug.println("Download "+d.getOtrKeyResult()+ " hinzugef�gt");
						OTRClient.getLog().println("Download "+d.getOtrKeyResult()+ " hinzugef�gt");
						download(d.getOtrKeyResult());
						wdownloads.remove(d);
					}
				}
				//CPU Entlastung
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		public void addDownload(Download d) {
			downloads.add(d);
		}
		
	}
	
	class BrowserCleaner extends Thread{
		public void run(){
			while(true){
				//Kopie des Browser Vektors
				Vector<Browser> tmpbrowsers = new Vector<Browser>(browsers);
				for(Browser b : tmpbrowsers){
					if(b.getDownloads() == 0 && (b.getLastUsage().getTime() <= System.currentTimeMillis()-5000) && b.isAvailable()){
						b.setAvailable(false);
						b.close();
					}
				}
				
				// CPU ENTLASTUNG
				try {
					Thread.sleep(60000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public DownloadWaiter getDKWaiter() {
		return dWaiter;
	}
}
