package core.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.common.io.Files;

import otr.CutlistSearcher;

public class ProcessRunner {

private CutlistSearcher cutlistSearcher;
	
	public ProcessRunner() {
		cutlistSearcher = new CutlistSearcher();
	}
	
	/**
	 * Startet den Dekodierprozess der �bergebenden Datei
	 * @param otrkey OTRKEY-Datei
	 */
	public void otrdecoder(File otrkey){
		
		OTRClient.getLog().println("Dekodiere : "+otrkey.getName());
		Debug.println("Dekodiere : "+otrkey.getName());
		
		String qotr = OTRClient.getConf().load("otrdir");
		File decodedir = new File(OTRClient.getConf().load("decodedir"));
		
		File cutlist = cutlistSearcher.cutlistdownload(cutlistSearcher.search(otrkey.getName().split(".otrkey")[0]));
		
		String cut = "";
		if(cutlist != null){
			cut = cutlist.getAbsolutePath();
		}else{
			System.out.println("Keine Cutliste verf�gbar speichere Datei in Manuelle Bearbeitung");
			decodedir = new File(OTRClient.getConf().load("decodemanualdir"));
		}
		
			try {
				String exec;
				if(isLinuxSystem()){
					exec = qotr+" -i "+otrkey.getAbsolutePath()+" -e duell10111@t-online.de -p Konsti.01 -o "+decodedir.getAbsolutePath()+" -C "+cut;
				}else{
					if(!cut.equals("")){
						exec = "\""+qotr+"\" -i \""+otrkey.getAbsolutePath()+"\" -e "+OTRClient.getConf().load("otr-email")+" -p "+OTRClient.getConf().load("otr-pw")+" -o \""+decodedir.getAbsolutePath()+"\" -C \""+cut+"\"";
					}else{
						exec = "\""+qotr+"\" -i \""+otrkey.getAbsolutePath()+"\" -e "+OTRClient.getConf().load("otr-email")+" -p "+OTRClient.getConf().load("otr-pw")+" -o \""+decodedir.getAbsolutePath()+"\"";
					}
				}
				OTRClient.getLog().println(exec);
				
				Process process = Runtime.getRuntime().exec(exec);
				
				new ProcessWriter(process.getInputStream(), process.getErrorStream(), 1);
				
				new OTRDecodeWatcher(otrkey, process,decodedir).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	
	/**
	 * @author Konstantin Sp�th
	 *
	 */
	class OTRDecodeWatcher extends Thread{
		private String name;
		private Process process;
		private File file;
		private File watchfile;
		public OTRDecodeWatcher(File f, Process process, File decodedir){
			this.process = process;
			name = f.getName().split(".otrkey")[0];
			file = f;
			watchfile = new File(decodedir.getAbsolutePath()+File.separator+name);
		}
		public void run(){
			try {
				process.waitFor();
				Thread.sleep(5000);
				for(int i = 0;i < 24;i++){
					if(watchfile.exists()){
						long partsize = watchfile.length();
						int num = 0;
						while(true){
							if(num == 3 && partsize == watchfile.length() && comparefilesize(file, watchfile)){
								break;
							}else if (num == 3){
								partsize = watchfile.length();
								num = -1;
							}
							num++;
						}
						file.delete();
						File confile = new File(OTRClient.getConf().load("mconvdir")+File.separator+watchfile.getName());
						try {
							if(watchfile.getAbsolutePath().contains(OTRClient.getConf().load("decodemanualdir"))) {
								OTRClient.getThreadManager().getClient().sendText("finished!"+name+".otrkey");
							}else {
								if(Boolean.parseBoolean(OTRClient.getConf().load("conv")))
									OTRClient.getThreadManager().getClient().sendText("finished!"+name+".otrkey");
								Files.move(watchfile, confile);
							}
						} catch (IOException e) {
							e.printStackTrace();
						}
						OTRClient.getLog().println("Download Datei gel�scht");
						break;
					}
					OTRClient.getLog().println("Decoder Datei nicht gefunden");
					Thread.sleep(5000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Konvertiert die �bergebende Datei in den H264 Format mit dem Expermientellen AAC Audioformat
	 * @param conv Zukonvertierende Datei
	 */
	public void fileconverter(File conv){
		
		if(conv == null){
			System.out.println("Fileconverter : File cant be null");
		}
		
		System.out.println("CONVERTER : "+conv.getName());
		
		if(isLinuxSystem()){
			try {
				String avconv = OTRClient.getConf().load("avconv");
				File convdir = new File(OTRClient.getConf().load("convdir"));
				
				String exec = null;
				
				if(Boolean.parseBoolean(OTRClient.getConf().load("libfdk"))){
					exec = avconv+" -i "+conv.getAbsolutePath()+" -movflags faststart -codec:v libx264 -preset medium -crf 20 -crf_max 23 -codec:a libfdk_aac "+convdir.getAbsolutePath()+File.separator+conv.getName()+".mp4";
				}else{
					exec = avconv+" -i "+conv.getAbsolutePath()+" -movflags faststart -codec:v libx264 -preset medium -crf 20 -codec:a aac -strict -2 "+convdir.getAbsolutePath()+File.separator+conv.getName()+".mp4";
				}
				
				Process process = Runtime.getRuntime().exec(exec);
				
				new ProcessWriter(process.getInputStream(), process.getErrorStream(),0);
				
				try {
					process.waitFor();
					Thread.sleep(10000);
					File src = new File(convdir+File.separator+conv.getName()+".mp4");
					if(!compare(src, conv)){
						src.delete();
						fileconverter(conv);
					}else{
						Files.move(src, new File(OTRClient.getConf().load("transdir")+File.separator+src.getName()));
						conv.delete();
						OTRClient.getThreadManager().getClient().sendText("finished!"+conv.getName());
						Debug.println("Fertig: "+conv.getName());
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(isWindowsSystem()){
			try {
				String avconv = OTRClient.getConf().load("avconv");
				File convdir = new File(OTRClient.getConf().load("convdir"));
				
				String exec = null;
				
				if(Boolean.parseBoolean(OTRClient.getConf().load("libfdk"))){
					exec = "\""+avconv+"\" -i \""+conv.getAbsolutePath()+"\" -movflags faststart -codec:v libx264 -profile:v high -preset "+OTRClient.getConf().load("convpreset")+" -crf 20 -crf_max 23 -codec:a libfdk_aac \""+convdir.getAbsolutePath()+File.separator+conv.getName()+".mp4\"";
				}else{
					exec = "\""+avconv+"\" -i \""+conv.getAbsolutePath()+"\" -movflags faststart -codec:v libx264 -profile:v high -preset "+OTRClient.getConf().load("convpreset")+" -crf 20 -codec:a aac -strict -2 \""+convdir.getAbsolutePath()+File.separator+conv.getName()+".mp4\"";
				}
				
				Process process = Runtime.getRuntime().exec(exec);
				
				new ProcessWriter(process.getInputStream(), process.getErrorStream(),0);
				
				try {
					process.waitFor();
					Thread.sleep(10000);
					File src = new File(convdir+File.separator+conv.getName()+".mp4");
					if(!compare(src, conv)){
						src.delete();
						fileconverter(conv);
					}else{
						conv.delete();
						Files.move(src, new File(OTRClient.getConf().load("transdir")+File.separator+src.getName()));
						OTRClient.getThreadManager().getClient().sendText("finished!"+src.getName());
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Vergleicht die Sourcedatei Zeitl�nge mit der zweiten Datei
	 * @param file1 Zweite Datei
	 * @param file2 Source
	 * @return true wenn sie um 2000 Millisekunden Toleranz die selbe Zeit haben sonst false
	 */
	public boolean compare(File file1, File file2){
		
		Date date1 = getDate(com(file1));
		Date date2 = getDate(com(file2));
		
		if((date1.getTime()-2000)<=date2.getTime()){
			return true;
		}
		return false;
	}
	
	private Date getDate(String input){
		DateFormat format = new SimpleDateFormat("HH:mm:ss");
		Date date;
		try {
			date = format.parse(input);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String com(File f){
		if(isLinuxSystem()){
		try {
			String avconv = OTRClient.getConf().load("avconv");
			Process prozess = Runtime.getRuntime().exec(new String[] { "sh", "-c", avconv+" -i "+f.getAbsolutePath()+" 2>&1 | grep 'Duration' | awk '{print $2}' | tr -d ,"});
			BufferedReader reader = new BufferedReader(new InputStreamReader(prozess.getInputStream()));
			String line = reader.readLine();
			System.out.println(line);
			return line.split("\\.")[0];
		} catch (IOException e) {
			e.printStackTrace();
		}catch(ArrayIndexOutOfBoundsException e){
			e.printStackTrace();
			e.printStackTrace(OTRClient.getLog().getWriter());
		}
		}else if(isWindowsSystem()){
			try {
				String avconv = OTRClient.getConf().load("avconv");
				String exec = avconv+" -i \""+f.getAbsolutePath()+"\"";
				Process prozess = Runtime.getRuntime().exec(exec);
				BufferedReader reader = new BufferedReader(new InputStreamReader(prozess.getErrorStream()));
				String tcomplete = "";
				String text = null;
				while((text = reader.readLine()) != null){
					tcomplete = tcomplete + text;
				}
				return tcomplete.split("Duration: ")[1].split(", start:")[0];
			} catch (IOException e) {
				e.printStackTrace();
			} catch(ArrayIndexOutOfBoundsException e){
				e.printStackTrace();
				e.printStackTrace(OTRClient.getLog().getWriter());
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param file1 Originaldatei
	 * @param file2 Dekodiertedatei
	 * @return True falls die Dateigr��en erkl�rbar sind (2/3 der originalgr��e besitzt)
	 */
	public boolean comparefilesize(File file1, File file2){
		double toleranz = 2/3;
		long minsize = (long) (file1.length()*toleranz);
		long decodesize = file2.length();
		
		if(minsize < decodesize){
			return true;
		}
		return false;
	}
	
	public static boolean isWindowsSystem()
	  { String osName = System.getProperty("os.name").toLowerCase();
	    return osName.indexOf("windows") >= 0;
	  }
	  
	 public static boolean isLinuxSystem()
	  { String osName = System.getProperty("os.name").toLowerCase();
	    return osName.indexOf("linux") >= 0;
	  }
	 
	 
class ProcessWriter{
		 
		 private InputStream err, in;
		 private PrintWriter writer;
		 
		 public ProcessWriter(InputStream in,InputStream err, int type){
			 this.err = err;
			 this.in = in;
			 DateFormat format = new SimpleDateFormat("HH-mm-ss");
			 DateFormat formatfolder = DateFormat.getDateInstance(DateFormat.SHORT);
			 String foldername = null;
			 switch(type){
			 case 0:
				 foldername = "avconv";
				 break;
			 case 1:
				 foldername = "otr";
				 break;
			 }
			 File folder = new File(OTRClient.getConf().load("logdir")+File.separator+formatfolder.format(new Date())+File.separator+foldername);
			 folder.mkdirs();
			 try {
				writer = new PrintWriter(new FileWriter(new File(folder.getAbsolutePath()+File.separator+"log_"+format.format(new Date())+".txt")));
				new ErrHandler().start();
				new OutHandler().start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		 }
		 
		 class ErrHandler extends Thread{
			 
			 public void run(){
				 InputStreamReader isr = new InputStreamReader(err);
				 BufferedReader reader = new BufferedReader(isr);
				 String line = null;
				 try {
					while((line = reader.readLine())!= null){
						 writer.println(line);
						 writer.flush();
					 }
				} catch (IOException e) {
					e.printStackTrace();
				}
			 }
			 
		 }
		 
		 class OutHandler extends Thread{
			 
			 public void run(){
				 InputStreamReader isr = new InputStreamReader(in);
				 BufferedReader reader = new BufferedReader(isr);
				 String line = null;
				 try {
					while((line = reader.readLine())!= null){
						 writer.println(line);
						 writer.flush();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			 }
			 
		 }
	 }
	
}
