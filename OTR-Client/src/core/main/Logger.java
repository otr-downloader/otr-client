package core.main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	private PrintWriter mainwriter;
	
	public Logger(){
		DateFormat formatfolder = DateFormat.getDateInstance(DateFormat.SHORT);
		DateFormat format = new SimpleDateFormat("HH-mm-ss");
		File dir = new File(OTRClient.getConf().load("logdir")+File.separator+formatfolder.format(new Date())+File.separator+"main");
		dir.mkdirs();
		try {
			File f = new File(dir.getAbsolutePath()+File.separator+"mainlog"+format.format(new Date())+".txt");
			f.createNewFile();
			mainwriter = new PrintWriter(new FileWriter(f));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Logger(String foldername, String logprefix){
		DateFormat formatfolder = DateFormat.getDateInstance(DateFormat.SHORT);
		DateFormat format = new SimpleDateFormat("HH-mm-ss");
		File dir = new File(OTRClient.getConf().load("logdir")+File.separator+formatfolder.format(new Date())+File.separator+foldername);
		dir.mkdirs();
		try {
			File f = new File(dir.getAbsolutePath()+File.separator+logprefix+format.format(new Date())+".txt");
			f.createNewFile();
			mainwriter = new PrintWriter(new FileWriter(f));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Druckt die übergebenden Text in die Hauptlogdatei und beginnt dann eine neue Zeile
	 * @param x Übergebende Text
	 */
	public void println(String x){
		mainwriter.println(x);
		mainwriter.flush();
	}
	
	/**
	 * Druckt die übergebenden Text in die Hauptlogdatei
	 * @param x Übergebende Text
	 */
	public void print(String x) {
		mainwriter.print(x);
		mainwriter.flush();
	}
	
	/**
	 * Gibt den Writer zurück
	 * @return
	 */
	public PrintWriter getWriter(){
		return mainwriter;
	}
	
}
