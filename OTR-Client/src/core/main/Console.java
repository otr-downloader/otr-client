package core.main;

import java.util.Scanner;

/**
 * Konsolenklasse
 * @author konst
 *
 */
public class Console implements Runnable{

	private Scanner scanner;
	
	public Console() {
		scanner = new Scanner(System.in);
	}
	
	@Override
	public void run() {
		System.out.println("Konsole betriebsbereit");
		while(true) {
			switch(scanner.nextLine()) {
			case "mkey":
				manualkey(1);
				break;
			default:
				System.out.println("Unbekannter Befehl");
				break;
			}
		}
	}
	
	
	/**
	 * Verschiedene Funktionen zu Manualkeys
	 * @param type Art der Funktion
	 */
	public void manualkey(int type) {
		switch (type) {
		case 1:
			while(true) {
			System.out.print("OTRKEY : ");
				String text = scanner.nextLine();
				if (!text.contains("exit") && !text.equals("") && text.length()>=3) {
					OTRClient.getThreadManager().getClient().sendText("mkey!"+text);
					break;
				}
			}
			break;
		}
	}
	
}
