package core.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

public class Config {

	private Properties config;
	
	private String path = System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrclient.properties";
	
	public Config(){
		config = new Properties();
		
		if(!new File(System.getProperty("user.dir")+File.separator+"config").exists()){
			new File(System.getProperty("user.dir")+File.separator+"config").mkdir();
		}
		if(!new File(path).exists() || !new File(System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrdownloader-otrkeys.txt").exists()){
			try {
				new File(path).createNewFile();
				new File(System.getProperty("user.dir")+File.separator+"config"+File.separator+"otrdownloader-otrkeys.txt").createNewFile();
				new File(System.getProperty("user.dir")+File.separator+"config"+File.separator+"hdsender.txt").createNewFile();
				save("downloadir", System.getProperty("user.dir"));
				save("profiledir", System.getProperty("user.dir"));
				save("otrdir", System.getProperty("user.dir")+File.separator+"otr"+File.separator+"otrdecoder");
				save("avconv", System.getProperty("user.dir")+File.separator+"avconv"+File.separator+"avconv");
				save("decodedir", System.getProperty("user.dir"));
				save("convdir", System.getProperty("user.dir"));
				save("transdir",System.getProperty("user.dir"));
				save("logdir", System.getProperty("user.dir")+File.separator+"log");
				save("cutlistdir",System.getProperty("user.dir"));
				save("decodemanualdir",System.getProperty("user.dir"));
				save("mconvdir",System.getProperty("user.dir"));
				save("geckodriver",System.getProperty("user.dir"));
				save("chromedriver",System.getProperty("user.dir"));
				save("browser","firefox");
				save("libfdk","false");
				save("ftphost","mediaftp.ddns.net");
				save("ftpuser", "testuser");
				save("ftppw", "testpw");
				save("ftpdir", "ftpdirectory");
				save("convpreset","medium");
				save("ftpactive","false");
				save("debug","false");
				save("otr-email","benutzer@test.de");
				save("otr-pw","test123");
				save("alloweddownloads", "4");
				save("allowedbrowsers", "1");
				save("allowedpoperations", "4");
				save("allowedconv", "2");
				save("serverip","127.0.0.1");
				save("directdownload","false");
				save("conv","true");
				System.out.println("Config einstellen !!!");
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			config.load(new FileInputStream(new File(path)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String load(String key){
		return config.getProperty(key);
	}
	
	public void save(String key,String wert){
		config.setProperty(key, wert);
		try {
			config.store(new FileOutputStream(new File(path)), "OTR Client Config");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Vector<String> DateiAuslesen(String path){
		Vector<String> names = new Vector<String>();
		
		File file = new File(path);
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			
			String text;
			while((text = reader.readLine())!= null){
				names.add(text);
			}
			
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return names;
	}
	
}
