package core.main;

public class Debug {

	public static void println(String msg){
		OTRClient.getLog().println("DEBUG: "+msg);
		if(Boolean.parseBoolean(OTRClient.getConf().load("debug"))){
			System.out.println(msg);
		}
	}
	
	public static void print(String msg){
		OTRClient.getLog().print("DEBUG: "+msg);
		if(Boolean.parseBoolean(OTRClient.getConf().load("debug"))){
			System.out.print(msg);
		}
	}
	
	public static void err(String msg){
		OTRClient.getLog().println("DEBUG-ERROR: "+msg);
		if(Boolean.parseBoolean(OTRClient.getConf().load("debug"))){
			System.err.println(msg);
		}
	}
	
}
