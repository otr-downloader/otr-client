package core.main;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

public class OTRClient {

	private static Logger logger;
	private static Config config;
	private static ProcessRunner processRunner;
	private static ThreadManager threadManager;
	
	//Browser Variables
	private static FirefoxOptions firefoxOptions;
	
	private static ChromeOptions chromeOptions;
	
	private static HashMap<WebDriver, String> chromedriver;

	private static Map<WebDriver, Integer> ids = new HashMap<WebDriver, Integer>();

	private static Vector<Integer> pids = new Vector<Integer>();
	
	// Version
	public static String version = "0.0.1";
	
	public static void main(String[] args) {
		new OTRClient();
	}
	
	public OTRClient() {
		config = new Config();
		logger = new Logger();
		if(Boolean.parseBoolean(config.load("debug")))
			Debug.println("Debugmodus aktiviert");
		processRunner = new ProcessRunner();
		threadManager = new ThreadManager();
		threadManager.getDownloadManager().availibility();
		
		//Browser Settings
		
		FirefoxProfile fpProfile = new FirefoxProfile();
		fpProfile.setPreference("browser.download.manager.showWhenStarting", false);
		fpProfile.setPreference("browser.download.dir", config.load("downloadir"));
		fpProfile.setPreference("browser.download.folderList", 2);
		fpProfile.setPreference("browser.helperApps.alwaysAsk.force", false);
		fpProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octetstream");
		
		firefoxOptions = new FirefoxOptions();
		firefoxOptions.setProfile(fpProfile);
		firefoxOptions.setHeadless(true);
		
		System.setProperty("webdriver.gecko.driver", config.load("geckodriver"));
		
		chromedriver = new HashMap<WebDriver,String>();
		
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", config.load("downloadir"));
		chromePrefs.put("browser.set_download_behavior", "{ behavior: 'allow'}");
		chromeOptions = new ChromeOptions();
		chromeOptions.setExperimentalOption("prefs", chromePrefs);
		//Aktuelle Fehler beim ChromeDriver V2.35
		chromeOptions.addArguments("--headless", "window-size=1024,768", "--no-sandbox");
		
		System.setProperty("webdriver.chrome.driver", config.load("chromedriver"));
		
	}
	
	/**
	 * Tester Konstruktor
	 * @param tester
	 */
	public OTRClient(boolean tester) {
		if(!tester) {
			new OTRClient();
			return;
		}
		
		config = new Config();
		logger = new Logger();
		
		//Browser Settings
		
		FirefoxProfile fpProfile = new FirefoxProfile();
		fpProfile.setPreference("browser.download.manager.showWhenStarting", false);
		fpProfile.setPreference("browser.download.dir", config.load("downloadir"));
		fpProfile.setPreference("browser.download.folderList", 2);
		fpProfile.setPreference("browser.helperApps.alwaysAsk.force", false);
		fpProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/octetstream,application/octet-stream");
		
		firefoxOptions = new FirefoxOptions();
		firefoxOptions.setHeadless(true);
		firefoxOptions.setProfile(fpProfile);
		
		System.setProperty("webdriver.gecko.driver", config.load("geckodriver"));
		
		chromedriver = new HashMap<WebDriver,String>();
		
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.default_directory", config.load("downloadir"));
		chromeOptions = new ChromeOptions();
		chromeOptions.setExperimentalOption("prefs", chromePrefs);
		
		System.setProperty("webdriver.chrome.driver", config.load("chromedriver"));
	}
	
	/**
	 * Close Client
	 */
	public static void exit() {
		System.out.println("Cient wird beendet");
		threadManager.getClient().close();
		System.exit(0);
	}
	
	/**
	 * Gibt einen neues WebDriver Objekt zur�ck
	 * 
	 * @return WebDriver
	 */
	public static WebDriver getnewBrowser() {
		WebDriver browser = null;
		try {
			browser = getBrowser();
		} catch (Exception e) {
			System.out.println("Browserfehler ... Starte neuen Browser");
			e.printStackTrace(logger.getWriter());
			killBrowser(browser);
			return getnewBrowser();
		}

		return browser;
	}

	private static WebDriver getBrowser() {
		switch(config.load("browser")){
		case "firefox":
			return getFirefoxDriver();
		case "chrome":
			return getChromeDriver();
		}
		System.out.println("Kein bekannter Browser eingestellt");
		System.exit(-1);
		return null;
	}
	
	private static WebDriver getChromeDriver(){
		ChromeDriver chromeDriver = new ChromeDriver(chromeOptions);
		String path = chromeDriver.getCapabilities().getCapability("chrome").toString().split("userDataDir=")[1].split("}")[0];
		WebDriver browser = chromeDriver;
		chromedriver.put(browser, path);
		return browser;
	}
	
	private static WebDriver getFirefoxDriver(){
		WebDriver browser = new FirefoxDriver(firefoxOptions);
		int id = -3;
		id = getProcID();
		ids.put(browser, id);
		return browser;
	}

	public static void killBrowser(WebDriver driver) {
		//Chromdedriver werden herrausgefiltert
		if(driver instanceof ChromeDriver){
			return;
		}
		int id = -3;
		try{
		id = ids.get(driver);
		}
		catch(NullPointerException e){
			logger.println("Browser not launched");
			return;
		}
		if (id != -1 && id != -3) {
			getLog().println("Alten Browser killen : " + id);
			if (ProcessRunner.isLinuxSystem()) {
				try {
					Runtime.getRuntime().exec("kill " + id);
				} catch (IOException e) {
					e.printStackTrace(getLog().getWriter());
				}
			}else if(ProcessRunner.isWindowsSystem()){
				try {
					Runtime.getRuntime().exec("taskkill /PID" + id + " /F");
				} catch (IOException e) {
					e.printStackTrace(getLog().getWriter());
				}
			}
		} else {
			getLog().println("ID konnte nicht herausgefunden werden (-1)");
		}
		ids.remove(driver);
	}

	/**
	 * Beendet den �bergebenden WebDriver
	 * @param driver
	 */
	public static void closeBrowser(WebDriver driver) {
		//Unn�tig ?
		/*for (String handle : driver.getWindowHandles()) {
			driver.switchTo().window(handle);
			driver.close();
		}*/
		
		driver.quit();
		//Browser Profil l�schen
		String path = chromedriver.get(driver);
		if(path == null){
			killBrowser(driver);
			ids.remove(driver);
			return;
		}
		File f = new File(path);
		if(f.exists()){
			try {
				FileUtils.deleteDirectory(f);
			} catch (IOException e) {
				System.out.println("L�schen der tempor�ren Daten fehlgeschlagen");
			}
		}
	}

	public static int getProcID() {

		if (ProcessRunner.isLinuxSystem()) {

			try {

				Process proc = Runtime.getRuntime().exec(new String[] { "pgrep", "-lf", "firefox" });
				BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

				String line, input = "";

				Vector<Integer> ids = new Vector<Integer>();

				while ((line = reader.readLine()) != null) {
					//Geckodriver �berspringen
					if(line.contains("geckodriver")){
						continue;
					}
					input = input.concat(line);
					ids.add(Integer.parseInt(input.split(" firefox")[0]));
				}

				getLog().println("---------------------------");

				for (int i : ids) {
					getLog().println("PID Liste TEST : " + i);
					if (!pids.contains(i)) {
						getLog().println("PID : " + i);
						pids = ids;
						return i;
					}
				}

				getLog().println("---------------------------");

				pids = ids;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return -1;
	}
	
	public static Logger getLog() {
		return logger;
	}

	public static Config getConf() {
		return config;
	}
	
	public static ThreadManager getThreadManager() {
		return threadManager;
	}
	
	public static ProcessRunner getProcessRunner() {
		return processRunner;
	}
}
