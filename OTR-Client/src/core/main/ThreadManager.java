package core.main;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import connector.Client;
import convert.MConvThread;
import downloader.DownloadManager;
import transport.TransportThread;

public class ThreadManager {

	private Client client;
	
	private DownloadManager downloadManager;
	
	private TransportThread transportThread;
	
	private MConvThread convThread;
	
	private Console console;
	
	ExecutorService executorService;
	
	public ThreadManager() {
		executorService = Executors.newFixedThreadPool(4);
		
		client = new Client();
		downloadManager = new DownloadManager();
		transportThread = new TransportThread();
		convThread = new MConvThread();
		console = new Console();
		
		executorService.execute(transportThread);
		executorService.execute(client);
		if(Boolean.parseBoolean(OTRClient.getConf().load("conv")))
			executorService.execute(convThread);
		executorService.execute(console);
	}
	
	public Client getClient() {
		return client;
	}
	
	public DownloadManager getDownloadManager() {
		return downloadManager;
	}
	
}
