package otr;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Comparator;
import java.util.Vector;

import core.main.OTRClient;

public class CutlistSearcher {
	
	public File cutlistdownload(CutlistResult c){
		if(c == null){
			return null;
		}
		try {
			String durl = "http://cutlist.at/getfile.php?id="+c.getCutlistid();
			URL url = new URL(durl);
			File file = null;
			
			HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
			
			int response = httpcon.getResponseCode();
			
			if(response == HttpURLConnection.HTTP_OK){
				String fileName = "";
	            String disposition = httpcon.getHeaderField("Content-Disposition");
	            String contentType = httpcon.getContentType();
	            int contentLength = httpcon.getContentLength();
	            
	            if (disposition != null) {
	                // extracts file name from header field
	                int index = disposition.indexOf("filename=");
	                if (index > 0) {
	                    fileName = disposition.substring(index + 10,
	                            disposition.length() - 1);
	                }
	            } else {
	                // extracts file name from URL
	                fileName = c.getName();
	            }
	            /*
	            OTRClient.getLog().println("Content-Type = " + contentType);
	            OTRClient.getLog().println("Content-Disposition = " + disposition);
	            OTRClient.getLog().println("Content-Length = " + contentLength);
	            OTRClient.getLog().println("fileName = " + fileName);
	            */
	            
	            InputStream in = httpcon.getInputStream();
	            String saveFilePath = OTRClient.getConf().load("cutlistdir")+File.separator+fileName;
	            
	            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
	            
	            int bytesRead = -1;
	            byte[] buffer = new byte[4096];
	            while((bytesRead = in.read(buffer))!= -1){
	            	outputStream.write(buffer, 0, bytesRead);
	            }
	            
	            outputStream.close();
	            in.close();
	            
	            //OTRClient.getLog().println("File downloaded");
	            
	            file = new File(saveFilePath);
			}else {
				OTRClient.getLog().println("No file to download. Server replied HTTP code: " + response);
				System.err.println("Cutlistdownload Error. Server replied HTTP code: " + response);
	        }
			
			httpcon.disconnect();
			return file;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public CutlistResult search(String name){
		String feedurl = "http://cutlist.at/getxml.php?name="+name;
		
		Vector<CutlistResult> research = new Vector<CutlistResult>();
		
		try {
			URLConnection openConn = new URL(feedurl).openConnection();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(openConn.getInputStream()));
			String feed = "";
			String inputLine = "";
			 while ((inputLine = reader.readLine()) != null){ 
		         //System.out.println(inputLine);
				 //OTRClient.getLog().println(inputLine);
		         feed = feed.concat(inputLine);
			 }
		     reader.close();
		     if(feed.equals("")){
		    	 return null;
		     }
		     String feed2 = feed.split("<cutlist",2)[1];
		     
		     for(String feedentry : feed2.split("<cutlist")){
		    	 CutlistResult cresult = new CutlistResult();
		    	 cresult.setName(feedentry.split("<name>")[1].split("</name>")[0]);
		    	 cresult.setCutlistid(Long.parseLong(feedentry.split("<id>")[1].split("</id>")[0]));
		    	 cresult.setDuration(Double.parseDouble(feedentry.split("<duration>")[1].split("</duration>")[0]));
		    	 cresult.setRatingcount(Integer.parseInt(feedentry.split("<ratingcount>")[1].split("</ratingcount>")[0]));
		    	 if(cresult.getRatingcount() != 0){
		    		 cresult.setRating(Double.parseDouble(feedentry.split("<rating>")[1].split("</rating>")[0]));
		    	 }else{
		    		 cresult.setRating(0);
		    	 }
		    	 cresult.setCuts(Integer.parseInt(feedentry.split("<cuts>")[1].split("</cuts>")[0]));
		    	 cresult.setAuthor(feedentry.split("<author>")[1].split("</author>")[0]);
		    	 cresult.setAuthorrating(Integer.parseInt(feedentry.split("<ratingbyauthor>")[1].split("</ratingbyauthor>")[0]));
		    	 cresult.setDownloads(Integer.parseInt(feedentry.split("<downloadcount>")[1].split("</downloadcount>")[0]));
		    	 cresult.setErrors(Integer.parseInt(feedentry.split("<errors>")[1].split("</errors>")[0]));
		    	 cresult.setDescription(feedentry.split("<usercomment>")[1].split("</usercomment>")[0]);
		    	 //System.out.println(cresult.toString());
		    	 research.add(cresult);
		     }
		     
		     //Auswahl
		     
		     CCompare compare = new CCompare();
		     
		     CutlistResult res = null;
		     
		     for(CutlistResult r : research){
		    	 if(res == null){
		    		 res = r;
		    	 }else{
		    		 if(compare.compare(res, r) == -1){
		    			 res = r;
		    		 }
		    	 }
		     }
		     //System.out.println(res.toString());  
		     OTRClient.getLog().println(res.toString());
		     return res;
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int getAnzahl(String name) {
		String feedurl = "http://cutlist.at/getxml.php?name="+name;
		
		int size = 0;
		
		try {
			URLConnection openConn = new URL(feedurl).openConnection();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(openConn.getInputStream()));
			String feed = "";
			String inputLine = "";
			 while ((inputLine = reader.readLine()) != null){ 
				 //OTRClient.getLog().println(inputLine);
		         feed = feed.concat(inputLine);
			 }
		     reader.close();
		     if(feed.equals("")){
		    	 return 0;
		     }
		     
		     String feed2 = feed.split("<cutlist",2)[1];
		     
		     for(String feedentry : feed2.split("<cutlist")){
		    	 size++;
		     }
		     
		     return size;
		     
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return 0;
	}
	
	public class CutlistResult{
		private String name;
		private long cutlistid;
		private double duration;
		private double rating;
		private int ratingcount;
		private int cuts;
		private int authorrating;
		private String author;
		private String description;
		private int downloads;
		private int errors;
		/**
		 * @return the cutlistid
		 */
		public long getCutlistid() {
			return cutlistid;
		}
		/**
		 * @param cutlistid the cutlistid to set
		 */
		public void setCutlistid(long cutlistid) {
			this.cutlistid = cutlistid;
		}
		/**
		 * @return the duration
		 */
		public double getDuration() {
			return duration;
		}
		/**
		 * @param duration the duration to set
		 */
		public void setDuration(double duration) {
			this.duration = duration;
		}
		/**
		 * @return the rating
		 */
		public double getRating() {
			return rating;
		}
		/**
		 * @param rating the rating to set
		 */
		public void setRating(double rating) {
			this.rating = rating;
		}
		/**
		 * @return the ratingcount
		 */
		public int getRatingcount() {
			return ratingcount;
		}
		/**
		 * @param ratingcount the ratingcount to set
		 */
		public void setRatingcount(int ratingcount) {
			this.ratingcount = ratingcount;
		}
		/**
		 * @return the cuts
		 */
		public int getCuts() {
			return cuts;
		}
		/**
		 * @param cuts the cuts to set
		 */
		public void setCuts(int cuts) {
			this.cuts = cuts;
		}
		/**
		 * @return the authorrating
		 */
		public int getAuthorrating() {
			return authorrating;
		}
		/**
		 * @param authorrating the authorrating to set
		 */
		public void setAuthorrating(int authorrating) {
			this.authorrating = authorrating;
		}
		/**
		 * @return the author
		 */
		public String getAuthor() {
			return author;
		}
		/**
		 * @param author the author to set
		 */
		public void setAuthor(String author) {
			this.author = author;
		}
		/**
		 * @return the downloads
		 */
		public int getDownloads() {
			return downloads;
		}
		/**
		 * @param downloads the downloads to set
		 */
		public void setDownloads(int downloads) {
			this.downloads = downloads;
		}
		/**
		 * @return the errors
		 */
		public int getErrors() {
			return errors;
		}
		/**
		 * @param errors the errors to set
		 */
		public void setErrors(int errors) {
			this.errors = errors;
		}
		
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String toString(){
			String re = "Name : "+getName()+"\nCutlist ID : "+getCutlistid()+"\nKommentar : "+getDescription()+"\nRating : "+getRating()+"\nRatingcount : "+getRatingcount()+"\nDuration : "+getDuration()+"\nCuts : "+getCuts()+"\nAutor : "+getAuthor()+"\nAutorbewertung : "+getAuthorrating()+"\nDownloads : "+getDownloads()+"\nErrors : "+getErrors()+"\n";
			return re;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	
	class CCompare implements Comparator<CutlistResult>{

		@Override
		public int compare(CutlistResult o1, CutlistResult o2) {
			if(o1.getErrors() > o2.getErrors()){
				return 1;
			}else if(o1.getErrors() < o2.getErrors()){
				return -1;
			}else if(o1.getRatingcount() > o2.getRatingcount() && o1.getRating() > o2.getRating()){
				return 1;
			}else if(o1.getRatingcount() < o2.getRatingcount() && o1.getRating() < o2.getRating()){
				return -1;
			}else if(o1.getRating() == o2.getRating()){
				if(o1.getDuration() > o2.getDuration() && ((!o1.getDescription().contains("Vorspan") || !o1.getDescription().contains("Abspan"))&& !o1.getDescription().contains("entfernt"))){
					return 1;
				}else if(o1.getDuration() < o2.getDuration() && ((!o2.getDescription().contains("Vorspan") || !o2.getDescription().contains("Abspan"))&& !o2.getDescription().contains("entfernt"))){
					return -1;
				}
			}else if(o1.getAuthorrating() > o2.getAuthorrating()){
				return 1;
			}else if(o1.getAuthorrating() < o2.getAuthorrating()){
				return -1;
			}else if(o1.getDownloads() > o2.getDownloads()){
				return 1;
			}else if(o1.getDownloads() < o2.getDownloads()){
				return -1;
			}
			return 0;
		}
		
	}
}
