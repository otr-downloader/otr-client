package convert;

import java.io.File;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import core.main.OTRClient;

public class MConvThread implements Runnable{

	Vector<String> manualconv;
	Vector<ConvThread> convThreads;
	ExecutorService executorService;
	
	public MConvThread() {
		manualconv = new Vector<String>();
		convThreads = new Vector<ConvThread>();
		int allconv = Integer.parseInt(OTRClient.getConf().load("allowedconv"));
		if(allconv == -1) {
			executorService = Executors.newCachedThreadPool();
		}else {
			executorService = Executors.newFixedThreadPool(allconv);
		}
	}
	
	@Override
	public void run() {
		while(true) {
			for(File f : new File(OTRClient.getConf().load("mconvdir")).listFiles()) {
				if(!mconvcontains(f)) {
					manualconv.add(f.getName());
					startconv(f);
				}
			}
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean mconvcontains(File f){
		for(String c : manualconv){
			if(c.equals(f.getName())){
				return true;
			}
		}
		return false;
	}
	
	private void startconv(File f) {
		ConvThread convThread = new ConvThread(f);
		convThreads.add(convThread);
		executorService.execute(convThread);
	}
	
	class ConvThread implements Runnable{
		
		private File f;
		
		public ConvThread(File f) {
			this.f = f;
		}
		
		@Override
		public void run() {
			OTRClient.getProcessRunner().fileconverter(f);
			convThreads.remove(this);
		}
		
	}

}
